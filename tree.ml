module StrSet = Set.Make(String)

type 'a tree =
  | Leaf of 'a
  | Node of 'a tree * 'a tree

let rec leaves_fold t s = match t with
  | Leaf v -> StrSet.add v s
  | Node (left, right) ->
    leaves_fold left (leaves_fold right s)

let rec leaves_merge = function
  | Leaf v -> StrSet.singleton v
  | Node (left, right) -> StrSet.union (leaves_merge left) (leaves_merge right)

type key_kind = Random | Incr | Decr

type traversal_kind = Fold | Merge

let random_key () = Random.int 10_000
let incr_key =
  let c = ref 0 in
  fun () -> incr c; !c
let decr_key =
  let c = ref 0 in
  fun () -> decr c; !c

let key kind = match kind with
  | Random -> random_key ()
  | Incr -> incr_key ()
  | Decr -> decr_key ()

let merge kind t =
  match kind with
  | Fold -> leaves_fold t StrSet.empty
  | Merge -> leaves_merge t

let rec mk_tree kind ~bound ~depth =
  if depth = 0 then Leaf (string_of_int (key kind))
  else
    let depth = depth - 1 in
    Node (mk_tree kind ~bound ~depth, mk_tree kind ~bound ~depth)

let key_kind_of_string = function
  | "random" -> Random
  | "incr" -> Incr
  | "decr" -> Decr
  | _ -> failwith "valid key kinds are: random, incr, decr"

let traversal_kind_of_string = function
  | "fold" -> Fold
  | "merge" -> Merge
  | _ -> failwith "valid traversal kinds are: fold, merge"

let () =
  match key_kind_of_string Sys.argv.(1),
        traversal_kind_of_string Sys.argv.(2),
        int_of_string Sys.argv.(3),
        int_of_string Sys.argv.(4)
  with
  | exception _ ->
    prerr_endline "Expected arguments: key_kind traversal_kind bound depth"
  | key_kind, traversal_kind, bound, depth ->
    let tree = mk_tree key_kind ~bound ~depth in
    for i = 0 to 10_000 do
      ignore (merge traversal_kind tree)
    done;

(* example runs:

$ time ./tree random fold 1000 10
real	0m3.480s
$ time ./tree incr fold 1000 10
real	0m3.110s
$ time ./tree decr fold 1000 10
real	0m3.301s

$ time ./tree random merge 1000 10
real	0m5.800s
$ time ./tree incr merge 1000 10
real	0m1.456s
$ time ./tree decr merge 1000 10
real	0m1.497s
*)
