set -x

ocamlopt -o tree tree.ml

time ./tree random fold 1000 10
# real	0m3.480s
time ./tree incr fold 1000 10
# real	0m3.110s
time ./tree decr fold 1000 10
# real	0m3.301s

time ./tree random merge 1000 10
# real	0m5.800s
time ./tree incr merge 1000 10
# real	0m1.456s

time ./tree decr merge 1000 10
# real	0m1.497s

